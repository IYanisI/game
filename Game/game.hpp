#ifndef Game
#define GAME

#include <cstdlib>
#include <ctime>
#include <iostream>
class Perso
{
protected :
	std::string m_name;
	int m_life;
	int m_gie;
	int m_power;
	int m_def;

public:
	
	Perso(std::string n, int l, int g, int p, int d);
	~Perso();

	int get_power();
	void set_power(int m_power);

	int get_def();
	void set_def(int m_def);

	int get_life();
	void set_life(int m_life);

	int get_gie();
	void set_gie(int m_gie);

	std::string get_name();
	void set_name(std::string m_name);

	int attack();
	int guard();

	int attack2();
	int guard2();

	int attack3();
	int guard3();

	void gielose(int lose);
	void gieadd(int add);
	void gieale(int add);
	int ulta();
	int ultd();
	void choixatk();
	void choixdef(int choicesave);
	int calcul(int atk, int def);
	virtual void Spirit();
	virtual void Die();
	virtual void PictureBase();
	virtual void PictureFinal();
	virtual void PictureLow();
	virtual std::string character(std::string chara);
	void reply();
	int atk(std::string choice, int deg);
	int def(std::string choice, int def);
};

class Hero : public Perso
{
	public:
	Hero(std::string n, int l, int g, int p, int d);
	~Hero();
	void Spirit();
	void Die();
	void PictureBase();
	void PictureFinal();
	void PictureLow();
	std::string character(std::string chara);
};


class Evil : public Perso
{
public:
	Evil(std::string name, int l, int g, int p, int d);
	~Evil();
	void Spirit();
	void Die();
	void PictureBase();
	void PictureFinal();
	void PictureLow();
	std::string character(std::string chara);
};
void PictureBarrage();
#endif
