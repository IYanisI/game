#include "game.hpp"

Perso::Perso(std::string n ,int l, int g, int p, int d) : m_name(n), m_life(l), m_gie(g), m_power(p), m_def(d)
{
}

Perso::~Perso()
{
}

Hero::Hero(std::string n, int l, int g, int p, int d) : Perso(n, l, g, p, d)
{
}

Hero::~Hero()
{
}

Evil::Evil(std::string n, int l, int g, int p, int d) : Perso(n, l, g, p, d)
{
}

Evil::~Evil()
{
}

int Perso::get_power()
{
	return m_power;
}

int Perso::get_def()
{
	return m_def;
}

int Perso::get_gie()
{
	return m_gie;
}

int Perso::get_life()
{
	return m_life;
}

void Perso::set_power(int m_power)
{
	if (this != nullptr)
	{
		this->m_power = m_power;
	}
}

void Perso::set_def(int m_def)
{
	if (this != nullptr)
	{
		this->m_def = m_def;
	}
}

void Perso::set_gie(int m_gie)
{
	if (this != nullptr)
	{
		this->m_gie = m_gie;
	}
}

void Perso::set_life(int m_life)
{
	if (this != nullptr)
	{
		this->m_life = m_life;
	}
}

void Perso::set_name(std::string m_name)
{
	if (this != nullptr)
	{
		this->m_name = m_name;
	}
}

std::string Perso::get_name()
{
	return m_name;
}

int Perso::attack()
{
	int g = m_power;
	return g;
}

int Perso::attack2()
{
	if (m_gie >= 15)
	{
		m_gie = m_gie - 15;
		int g = m_power * 1.5;
		return g;
	}
	else
	{
		srand((unsigned int)time(0));

		int valeur = rand() % m_gie +1;
		m_gie = m_gie - valeur;
		int g = m_power +(valeur / 2) ;
		std::cout << "vous n'infligerez que " << g << " de degat car vous n'avez plus assez de Gie" << std::endl;
		return g;
	}
}

int Perso::attack3()
{
	if (m_gie >= 30)
	{
		m_gie = m_gie - 30;
		int g = m_power * 2;
		return g;
	}
	else
	{
		srand((unsigned int)time(0));
		int valeur = rand() % m_gie +1;
		m_gie = m_gie - valeur;
		int g = m_power + (valeur / 2);
		std::cout << "vous n'infligerez que " << g << " de degat car vous n'avez plus assez de Gie" << std::endl;
		return g;
	}
}

int Perso::guard()
{
	int g = m_def;
	return g;
}

int Perso::guard2()
{
	if (m_name == "Le Stagiaire de l'ombre")
	{
		if (m_gie >= 15)
		{
			m_gie = m_gie - 15;
			return -1;
		}
		else
		{
			m_gie = 0;
			return 1;
		}
	}
	else if (m_gie >= 15)
	{
		m_gie = m_gie - 15;
		int g = m_def * 1.5;
		return g;
	}
	else
	{
		srand((unsigned int)time(0));

		int valeur = rand() % m_gie + 1;
		m_gie = m_gie - valeur;
		int g = m_def + (valeur / 2);
		std::cout << "vous allez vous proteger de " << g << " degat car vous n'avez plus assez de Gie" << std::endl;
		return g;
	}
}

int Perso::guard3()
{
	if (m_gie >= 30)
	{
		m_gie = m_gie - 30;
		int g = m_def * 2;
		return g;
	}
	else
	{
		srand((unsigned int)time(0));
		int valeur = rand() % m_gie +1;
		m_gie = m_gie - valeur;
		int g = m_def + (valeur / 2);
		std::cout << "vous allez vous proteger de " << g << " degat car vous n'avez plus assez de Gie" << std::endl;
		return g;
	}
}

void Perso::gielose(int lose)
{
	m_gie = m_gie - lose;
}

void Perso::gieale(int add)
{
	if (m_name == "Omni man")
	{
		srand((unsigned int)time(0));

		int valeur = rand() % 5 + 1;
		if (valeur == 1)
		{
			printf("Alors on faiblit, je pourrais me battre pendant des siecle.\n");
		}
		else if (valeur == 2)
		{
			printf("Comment tu t'appelle...\nnon en fait sa n'a aucune importance, tu n'es pas assez exceptionnelle pour ca.\n");
		}
		else if (valeur == 3)
		{
			printf("Tu n'as pas eu de chance d'etre tomber sur un viltrumite.\n");
		}
		else if (valeur == 4)
		{
			printf("Tu aura beau etre le plus puissant de cette planete, les viltrumite te depasseront.\n");
		}
		else if (valeur == 5)
		{
			printf("je pensais t'epargner, si tu t'etais soumis a viltrum.\n");
		}
	}
	else
	{
		srand((unsigned int)time(0));
		int valeur = rand() % add + 1;
		m_gie = m_gie + valeur;
		if (m_gie >= 201)
		{
			m_gie = 200;
		}
	}
}

void Perso::gieadd(int add)
{
	m_gie = m_gie + add;
	if (m_gie >= 201)
	{
		m_gie = 200;
	}
	if (m_name == "Omni man")
	{
		m_gie = 999;
	}
}

int Perso::ultd()
{
	if (m_gie >= 200)
	{
		int g = m_def * 10;
		m_gie = -10;
		return g;
	}
	else if (m_gie < 200)
	{
		return -97821;
	}

}

int Perso::ulta()
{
	if (m_gie >= 200)
	{
		int g = m_power * 10;
		m_gie = -10;
		return g;
	}
	else if (m_gie < 200)
	{
	return -97821;
	}
	
}

void Perso::choixdef(int choicesave)
{
	if (m_name == "Omni man"&& choicesave == 0)
	{
		std::cout << "+------------+ +------------+ *---------------*" << std::endl;
		std::cout << "| 1.Defendre | | 2.Provoque | | 3.DEF Special |" << std::endl;
		std::cout << "+------------+ +------------+ *---------------*" << std::endl;
	}
	else if (m_name == "Omni man"&& choicesave == 1)
	{
		std::cout << "+------------+ +------------+ *---------------*" << std::endl;
		std::cout << "| 1.Attaquer | | 2.Provoque | | 3.ATK Special |" << std::endl;
		std::cout << "+------------+ +------------+ *---------------*" << std::endl;
	}
	else if (choicesave == 1)
	{
		std::cout << "+------------+ +-------------+ *---------------*" << std::endl;
		std::cout << "| 1.Attaquer | | 2.Recouvrer | | 3.ATK Special |" << std::endl;
		std::cout << "+------------+ +-------------+ *---------------*" << std::endl;
	}
	else
	{
		std::cout << "+------------+ +-------------+ *---------------*" << std::endl;
		std::cout << "| 1.Defendre | | 2.Recouvrer | | 3.DEF Special |" << std::endl;
		std::cout << "+------------+ +-------------+ *---------------*" << std::endl;
	}
}

void Perso::choixatk()
{
	if (m_name == "Omni man")
	{
		std::cout << "+------------+ +------------+ *---------------*" << std::endl;
		std::cout << "| 1.Attaquer | | 2.Provoque | | 3.ATK Special |" << std::endl;
		std::cout << "+------------+ +------------+ *---------------*" << std::endl;
	}
	else
	{
		std::cout << "+------------+ +-------------+ *---------------*" << std::endl;
		std::cout << "| 1.Attaquer | | 2.Recouvrer | | 3.ATK Special |" << std::endl;
		std::cout << "+------------+ +-------------+ *---------------*" << std::endl;
	}
}

int Perso::calcul(int deg, int def)
{
	if (m_name == "Le Stagiaire de l'ombre")
	{
		if (def == -1)
		{
			std::cout << m_name << "a esquivee " << deg << " de degats." << std::endl;
		}
		else if (def == 1)
		{
			srand((unsigned int)time(0));
			int valeur = rand() % deg + 1;
			m_life = m_life - (valeur /2) ;
			std::cout << "En esquivant, vous vous etes pris l'attaque.\nVous avez recu " << valeur/2 << " de degats." << std::endl;
		}
		else
		{
			m_life = m_life - (deg / 1.5);
			std::cout << m_name << " recoit " << deg /1.5 << " de degats." << std::endl;
		}
	}
	else if (2 * deg - def < -1)
	{
		std::cout << m_name << " recoit " << 0 << " de degats." << std::endl;
	}
	else
	{
		m_life = m_life - (2 * deg - def) * 0.5;
		
		if (2 * deg - def * 0.5 != 0)
		{
			std::cout << m_name << " recoit " << (2 * deg - def) * 0.5 << " de degats." << std::endl;
		}
	}
		Die();
	if (m_life <= 0)
	{
		return 0;
	}
}

void Perso::Spirit()
{
}
void Perso::Die()
{
}
void Hero::Spirit()
{
	if (m_name == "Super Salarial")
	{
		m_power = 10 + (300 - m_life) / 8;
	}
}

void Evil::Spirit()
{
	if (m_name == "Le Patronat")
	m_def = 10 + (300 - m_life) / 8;
}

void Evil::Die()
{
	if (m_life <= 0)
	{
		if (m_name == "Le Patronat")
		{
			m_life = 500;
				m_name = "Le Patriarcal";
				m_power = 65;
				m_def = 45;
				printf("Tu croyais vraiment m'avoir tuer ?\n");
				PictureBase();
		}
	}
}

void Hero::Die()
{

	if (m_life <= 0)
	{
		if (m_name == "Egalitaria")
		{
			srand((unsigned int)time(0));
			int valeur = rand() % 3 + 1;
			if (valeur == 1)
			{
				m_life = 1;
				printf("Je refuse de mourir dans un monde inéquitable.");
			}
	}
	}
}

int Perso::atk(std::string choice, int deg )
{
	while (true)
	{
		printf("1.Puissance 1 = 0 Gie\n2.Puissance 1.5 = 15 Gie\n3.Puissance 2 = 30 Gie\n");
		std::cin >> choice;
		if (choice == "1")// 0
		{
			system("cls");
			deg = attack();
			break;
		}
		else if (choice == "2")// 2
		{
			system("cls");
			deg = attack2();
			break;
		}
		else if (choice == "3")// 3
		{
			system("cls");
			deg = attack3();
			break;
		}
	}
	return deg;
}

int Perso::def(std::string choice, int def)
{
	if (m_name == "Le Stagiaire de l'ombre")
	{
		while (true)
		{
			printf("1.Resistance 1 = 0 Gie\n2.Esquive = 15 Gie\n");
			std::cin >> choice;
			if (choice == "1")// 0
			{
				system("cls");
				def = guard();
				break;
			}
			else if (choice == "2")// 2
			{
				system("cls");
				def = guard2();
				break;
			}
		}
	}
	else
	{
		while (true)
		{
			printf("1.Resistance 1 = 0 Gie\n2.Resistance 1.5 = 15 Gie\n3.Resistance 2 = 30 Gie\n");
			std::cin >> choice;
			if (choice == "1")// 0
			{
				system("cls");
				def = guard();
				break;
			}
			else if (choice == "2")// 2
			{
				system("cls");
				def = guard2();
				break;
			}
			else if (choice == "3")// 3
			{
				system("cls");
				def = guard3();
				break;
			}
		}
	}
	return def;
}
